import React, { Component } from 'react';
import { SlotItemList, HintItemList, BordItemList, EventList, LogList} from '../Components'
import { connect } from "react-redux";
import { fetchgetWheelConfiguration, fetchlastNSpinStatistics, fetchgetNextGame, fetchgetSpinByID } from '../Redux/Action/Action';
import Loader from 'react-loader-spinner';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"

const styles = {
    wrapperStyle: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    }
}

class LandingPage extends Component {

  constructor(props) {
    super(props)

    this.state = {
        logList: [],
        eventList: [],
        timer: 0,
        seconds: 50,
    }
  }

  startTimer = () => {
    this.myInterval = setInterval(() => {
        const { seconds } = this.state
        const { getNextGame } = this.props;

        if (seconds > 0) {

            this.setState(({ seconds }) => ({
                seconds: seconds - 1
            }))
        }else{
            this.props.handleFetchgetSpinByID(getNextGame.data.id)
        }
        
    }, 1000)
  }

  componentDidMount(){
    clearInterval(this.myInterval)
    const { logList } = this.state;
    this.props.handleFetchgetWheelConfiguration();
    this.props.handleFetchgetNextGame();
    this.props.handleFetchlastNSpinStatistics();
    const value = [...logList, `${new Date().toISOString()} Loading game board`, `${new Date().toISOString()} GET .../configuration`]
    this.setState({logList: value })
  }

  componentWillUnmount() {
    clearInterval(this.myInterval)
}

  componentDidUpdate(prevProps){
    const { getWheelConfiguration, lastNSpinStatistics, getNextGame, getSpinbyID } = this.props;
    const { eventList, logList } = this.state;
    if(getWheelConfiguration.loading && !prevProps.getWheelConfiguration.loading){
        const logListData = [...logList, `${new Date().toISOString()} Checking for new game`]
        this.setState({logList: logListData })
    }

    if(lastNSpinStatistics.success && !prevProps.lastNSpinStatistics.success){
        const logListData = [...logList, `${new Date().toISOString()} GET .../stats?limit=200`]
        this.setState({logList: logListData })
    }

    if(getNextGame.success && !prevProps.getNextGame.success){
        const logListData = [...logList, `${new Date().toISOString()} sleeping for fakeStartDelta ${getNextGame.data.fakeStartDelta} sec`, `${new Date().toISOString()} GET .../nextGame`, ]
        
        this.setState({logList: logListData, seconds: getNextGame.data.fakeStartDelta  }, ()=> this.startTimer())
    }

    if(getSpinbyID.loading && !prevProps.getSpinbyID.loading){
        const logListData = [...logList, `${new Date().toISOString()} Spinning the wheel`, `${new Date().toISOString()} Wheel is already spinning ;`];

        this.setState({logList: logListData })
    }
    
    if(getSpinbyID.success && !prevProps.getSpinbyID.success){
        console.log(getNextGame.data.result)
        clearInterval(this.myInterval)
        const logListData = [...logList, `${new Date().toISOString()} GET .../game/${getNextGame.data.id}`, `${new Date().toISOString()} Stopping the wheel`];
        const eventListData = getSpinbyID.data.result? [...eventList, `Game  ${getSpinbyID.data.id} ended, result is ${getSpinbyID.data.result}` ]: eventList;
        this.props.handleFetchgetNextGame();
        this.props.handleFetchlastNSpinStatistics();
        this.setState({logList: logListData, eventList: eventListData })
    }

  }


  render() {
    const { getWheelConfiguration, getNextGame, lastNSpinStatistics, getSpinbyID } = this.props;
    const { eventList, logList, seconds  } = this.state;
    return (
        <div style={styles.wrapperStyle}>
            <div>
                <h1>Aardvark Roulette game API demo</h1>
                <form role="form">
                    <div className="form-group">
                        <label htmlFor="api_base">API base URL</label>
                        <input className="form-control" id="api_base" onChange={()=>{}} value="https://dev-games-backend.advbet.com/v1/ab-roulette/1/" />
                    </div>	
                </form>
                <h4>Stats (last 200)</h4>
                <div style={{display: 'flex', justifyContent: 'space-between' }}>
                    <div style={{display: 'flex'}}>
                        <div style={{width: '140px', marginLeft: '40px', backgroundColor:'#D9EDF7', textAlign: 'center'}}>
                            <h6> Cold </h6>
                        </div>
                        <div>
                            <h6> Normal </h6>
                        </div>
                    </div>
                    <div style={{width: '140px', marginRight: '5px', backgroundColor:'#F2DEDE', textAlign: 'center'}}>
                        <h6> Hot </h6>
                    </div>
                </div>
                {
                    lastNSpinStatistics.success && getWheelConfiguration.success && (
                        <div>
                            <SlotItemList items={lastNSpinStatistics.data} colors={getWheelConfiguration.data.colors} />
                            <HintItemList items={lastNSpinStatistics.data} />
                        </div>
                    )
                }
                
                
                <div style={{display: 'flex', justifyContent: 'space-between', width: '100%'}}>
                    <div style={{width: '50%'}}>
                        <h4>Game Board</h4>
                        {   getWheelConfiguration.success && (
                             <div style={{width: '450px'}}>
                                <BordItemList items={getWheelConfiguration.data.positionToId} colors={getWheelConfiguration.data.colors} />
                            </div>)
                        }
                        <h4>Events</h4>
                        <EventList items={eventList} time={seconds} game={getNextGame.data.id} />`
                    </div>
                    <div style={{ marginTop: '35px'}}>
                    {
                        getSpinbyID.loading && (
                            <Loader
                                type="Oval"
                                color="Black"
                                height={38}
                                width={38}
                            />
                        )
                    }
                       
                    </div>
                    <div style={{width: '42%'}}>
                        <h4>Log</h4>
				        <LogList items={logList} />
                    </div>
                </div>
               
            </div>
        </div>
    );
  }
}

  
const mapDispatchToProps = dispatch => ({
    handleFetchgetWheelConfiguration: () => {
        dispatch(fetchgetWheelConfiguration(1));
    },
    handleFetchlastNSpinStatistics: () => {
        dispatch(fetchlastNSpinStatistics(1));
    },
    handleFetchgetNextGame: () => {
        dispatch(fetchgetNextGame(1));
    },
    handleFetchgetSpinByID: (id) => {
        dispatch(fetchgetSpinByID(1,id));
    },

    
});

function mapStateToProps(state) {
    console.log(state)
  return {
    getWheelConfiguration: state.data.getWheelConfiguration,
    lastNSpinStatistics: state.data.lastNSpinStatistics,
    getNextGame: state.data.getNextGame,
    getSpinbyID: state.data.getSpinbyID
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LandingPage);