import React from 'react';
import LandingPage from './Features/LandingPage';
import { Provider } from 'react-redux';
import store from './Redux/Store/Store';
import 'bootstrap/dist/css/bootstrap.css';

function App() {
  return (
    <Provider store={store}>
      <LandingPage />
    </Provider>

  );
}

export default App;
