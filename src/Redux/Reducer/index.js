import { combineReducers } from 'redux';
import landingReducer from './Reducer'

const createRootReducers = () =>
  combineReducers({
    data: landingReducer
  });
  
export default createRootReducers;