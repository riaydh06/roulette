import { 
  GET_SPIN_BY_UUID_REQUEST, 
  GET_SPIN_BY_UUID_SUCCESS, 
  GET_SPIN_BY_UUID_FAILED,
  GET_SPIN_BY_ID_REQUEST,
  GET_SPIN_BY_ID_SUCCESS,
  GET_SPIN_BY_ID_FAILED,
  GET_NEXT_GAME_REQUEST,
  GET_NEXT_GAME_SUCCESS,
  GET_NEXT_GAME_FAILED,
  GET_LAST_N_FINISHED_SPINS_REQUEST,
  GET_LAST_N_FINISHED_SPINS_SUCCESS,
  GET_LAST_N_FINISHED_SPINS_FAILED,
  GET_LIST_OF_UPCOMING_SPINS_REQUEST,
  GET_LIST_OF_UPCOMING_SPINS_SUCCESS,
  GET_LIST_OF_UPCOMING_SPINS_FAILED,
  LAST_N_SPIN_STATISTICS_REQUEST,
  LAST_N_SPIN_STATISTICS_SUCCESS,
  LAST_N_SPIN_STATISTICS_FAILED,
  GET_WHEEL_CONFIGURATION_REQUEST,
  GET_WHEEL_CONFIGURATION_SUCCESS,
  GET_WHEEL_CONFIGURATION_FAILED
} from '../Constant/ActionType';
  import initialState from '../Constant/InitialState';
  
  export default (state = initialState, action) => {
    switch (action.type) {
  
      // get_spin_by_uuid
      case GET_SPIN_BY_UUID_REQUEST:
        return {
            ...state,
            getSpinbyUUID:{
              loading: true,
              success: false,
              failed: false,
              data: {}
            }
        };
      case GET_SPIN_BY_UUID_SUCCESS:
        return {
          ...state,
          getSpinbyUUID:{
            loading: false,
            success: true,
            failed: false,
            data: action.response.data
          }
        };
      case GET_SPIN_BY_UUID_FAILED:
        return {
          ...state,
          getSpinbyUUID:{
            loading: false,
            success: false,
            failed: true,
            data: {}
          }
        };

      // get_spin_by_id
      case GET_SPIN_BY_ID_REQUEST:
        return {
            ...state,
            getSpinbyID:{
              loading: true,
              success: false,
              failed: false,
              data: {}
            }
        };
      case GET_SPIN_BY_ID_SUCCESS:
        return {
          ...state,
          getSpinbyID:{
            loading: false,
            success: true,
            failed: false,
            data: action.response.data
          }
        };
      case GET_SPIN_BY_ID_FAILED:
        return {
          ...state,
          getSpinbyID:{
            loading: false,
            success: false,
            failed: true,
            data: {}
          }
        };
      
      // get_next_game
      case GET_NEXT_GAME_REQUEST:
        return {
            ...state,
            getNextGame:{
              loading: true,
              success: false,
              failed: false,
              data: {}
            }
        };
      case GET_NEXT_GAME_SUCCESS:
        return {
          ...state,
          getNextGame:{
            loading: false,
            success: true,
            failed: false,
            data: action.response.data
          }
        };
      case GET_NEXT_GAME_FAILED:
        return {
          ...state,
          getNextGame:{
            loading: false,
            success: false,
            failed: true,
            data: {}
          }
        };

      // get_last_n_finished_spins
      case GET_LAST_N_FINISHED_SPINS_REQUEST:
        return {
            ...state,
            getLastNFinishedSpins:{
              loading: true,
              success: false,
              failed: false,
              data: {}
            }
        };
      case GET_LAST_N_FINISHED_SPINS_SUCCESS:
        return {
          ...state,
          getLastNFinishedSpins:{
            loading: false,
            success: true,
            failed: false,
            data: action.response.data
          }
        };
      case GET_LAST_N_FINISHED_SPINS_FAILED:
        return {
          ...state,
          getLastNFinishedSpins:{
            loading: false,
            success: false,
            failed: true,
            data: {}
          }
        };

       // get_list_of_upcoming_spins
       case GET_LIST_OF_UPCOMING_SPINS_REQUEST:
       return {
           ...state,
           getListofUpcomingSpins:{
             loading: true,
             success: false,
             failed: false,
             data: {}
           }
       };
     case GET_LIST_OF_UPCOMING_SPINS_SUCCESS:
       return {
         ...state,
         getListofUpcomingSpins:{
           loading: false,
           success: true,
           failed: false,
           data: action.response.data
         }
       };
     case GET_LIST_OF_UPCOMING_SPINS_FAILED:
       return {
         ...state,
         getListofUpcomingSpins:{
           loading: false,
           success: false,
           failed: true,
           data: {}
         }
       };

       // last_n_spin_statistics
       case LAST_N_SPIN_STATISTICS_REQUEST:
       return {
           ...state,
           lastNSpinStatistics:{
             loading: true,
             success: false,
             failed: false,
             data: []
           }
       };
     case LAST_N_SPIN_STATISTICS_SUCCESS:
       return {
         ...state,
         lastNSpinStatistics:{
           loading: false,
           success: true,
           failed: false,
           data: action.response.data
         }
       };
     case LAST_N_SPIN_STATISTICS_FAILED:
       return {
         ...state,
         lastNSpinStatistics:{
           loading: false,
           success: false,
           failed: true,
           data: []
         }
       };

       // get_wheel_configuration
       case GET_WHEEL_CONFIGURATION_REQUEST:
       return {
           ...state,
           getWheelConfiguration:{
             loading: true,
             success: false,
             failed: false,
             data: {}
           }
       };
     case GET_WHEEL_CONFIGURATION_SUCCESS:
       return {
         ...state,
         getWheelConfiguration:{
           loading: false,
           success: true,
           failed: false,
           data: action.response.data
         }
       };
     case GET_WHEEL_CONFIGURATION_FAILED:
       return {
         ...state,
         getWheelConfiguration:{
           loading: false,
           success: false,
           failed: true,
           data: {}
         }
       };
  
      
      default:
        return state;
    }
  };