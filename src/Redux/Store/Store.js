import createRootReducers from '../Reducer';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import initialState from '../Constant/InitialState';

const store = createStore(
  createRootReducers(),
  initialState,
  applyMiddleware(thunk)
);

export default store;
