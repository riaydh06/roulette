const initialState = {
    getSpinbyUUID: {
        loading: false,
        success: false,
        failed: false,
        data: {}
    },
    getSpinbyID: {
        loading: false,
        success: false,
        failed: false,
        data: {}
    },
    getNextGame: {
        loading: false,
        success: false,
        failed: false,
        data: {}
    },
    getLastNFinishedSpins: {
        loading: false,
        success: false,
        failed: false,
        data: {}
    },
    getListofUpcomingSpins: {
        loading: false,
        success: false,
        failed: false,
        data: {}
    },
    lastNSpinStatistics: {
        loading: false,
        success: false,
        failed: false,
        data: []
    },
    getWheelConfiguration: {
        loading: false,
        success: false,
        failed: false,
        data: {}
    },
    versionInformation: {
        loading: false,
        success: false,
        failed: false,
        data: {}
    }
}

export default initialState;