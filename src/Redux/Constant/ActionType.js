export const GET_SPIN_BY_UUID_REQUEST = 'get_spin_by_uuid_request';
export const GET_SPIN_BY_UUID_SUCCESS = 'get_spin_by_uuid_success';
export const GET_SPIN_BY_UUID_FAILED = 'get_spin_by_uuid_failed';

export const GET_SPIN_BY_ID_REQUEST = 'get_spin_by_id_request';
export const GET_SPIN_BY_ID_SUCCESS = 'get_spin_by_id_success';
export const GET_SPIN_BY_ID_FAILED = 'get_spin_by_id_failed';

export const GET_NEXT_GAME_REQUEST = 'get_next_game_request';
export const GET_NEXT_GAME_SUCCESS = 'get_next_game_success';
export const GET_NEXT_GAME_FAILED = 'get_next_game_failed';

export const GET_LAST_N_FINISHED_SPINS_REQUEST = 'get_last_n_finished_spins_request';
export const GET_LAST_N_FINISHED_SPINS_SUCCESS = 'get_last_n_finished_spins_success';
export const GET_LAST_N_FINISHED_SPINS_FAILED = 'get_last_n_finished_spins_failed';

export const GET_LIST_OF_UPCOMING_SPINS_REQUEST = 'get_list_of_upcoming_spins_request';
export const GET_LIST_OF_UPCOMING_SPINS_SUCCESS = 'get_list_of_upcoming_spins_success';
export const GET_LIST_OF_UPCOMING_SPINS_FAILED = 'get_list_of_upcoming_spins_failed';

export const LAST_N_SPIN_STATISTICS_REQUEST = 'last_n_spin_statistics_request';
export const LAST_N_SPIN_STATISTICS_SUCCESS = 'last_n_spin_statistics_success';
export const LAST_N_SPIN_STATISTICS_FAILED = 'last_n_spin_statistics_failed';

export const GET_WHEEL_CONFIGURATION_REQUEST = 'get_wheel_configuration_request';
export const GET_WHEEL_CONFIGURATION_SUCCESS = 'get_wheel_configuration_success';
export const GET_WHEEL_CONFIGURATION_FAILED = 'get_wheel_configuration_failed';

export const VERSION_INFORMATION_REQUEST = 'version_information_request';
export const VERSION_INFORMATION_SUCCESS = 'version_information_success';
export const VERSION_INFORMATION_FAILED = 'version_information_failed';


