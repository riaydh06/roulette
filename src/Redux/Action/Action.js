import { axiosInstance } from '../../Config';
import { 
  GET_SPIN_BY_UUID_REQUEST, 
  GET_SPIN_BY_UUID_SUCCESS, 
  GET_SPIN_BY_UUID_FAILED,
  GET_SPIN_BY_ID_REQUEST,
  GET_SPIN_BY_ID_SUCCESS,
  GET_SPIN_BY_ID_FAILED,
  GET_NEXT_GAME_REQUEST,
  GET_NEXT_GAME_SUCCESS,
  GET_NEXT_GAME_FAILED,
  GET_LAST_N_FINISHED_SPINS_SUCCESS,
  GET_LAST_N_FINISHED_SPINS_FAILED,
  GET_LIST_OF_UPCOMING_SPINS_REQUEST,
  GET_LIST_OF_UPCOMING_SPINS_SUCCESS,
  GET_LIST_OF_UPCOMING_SPINS_FAILED,
  LAST_N_SPIN_STATISTICS_REQUEST,
  LAST_N_SPIN_STATISTICS_SUCCESS,
  GET_WHEEL_CONFIGURATION_REQUEST,
  GET_WHEEL_CONFIGURATION_SUCCESS,
  GET_WHEEL_CONFIGURATION_FAILED,
  GET_LAST_N_FINISHED_SPINS_REQUEST,
  LAST_N_SPIN_STATISTICS_FAILED
} from '../Constant/ActionType';

// get_spin_by_uuid
export const getSpinByUUIDRequest = () => ({
  type: GET_SPIN_BY_UUID_REQUEST
});

export const getSpinByUUIDSuccess = response => ({
  type: GET_SPIN_BY_UUID_SUCCESS,
  response
});

export const getSpinByUUIDFailed = error => ({
  type: GET_SPIN_BY_UUID_FAILED,
  error
});

export const fetchgetSpinByUUID = (id=1,uuid) => (dispatch) => {
  dispatch(getSpinByUUIDRequest());
  axiosInstance
    .get('/'+id+'/game/'+uuid)
    .then((res) => {
      console.log(res)
      dispatch(getSpinByUUIDSuccess(res));
    })
    .catch((e) => {
      console.log(e.response)
      dispatch(getSpinByUUIDFailed(e));
    });
};

// get_spin_by_id
export const getSpinByIDRequest = () => ({
  type: GET_SPIN_BY_ID_REQUEST
});

export const getSpinByIDSuccess = response => ({
  type: GET_SPIN_BY_ID_SUCCESS,
  response
});

export const getSpinByIDFailed = error => ({
  type: GET_SPIN_BY_ID_FAILED,
  error
});

export const fetchgetSpinByID = (id=1,gameId) => (dispatch) => {
  dispatch(getSpinByIDRequest());
  axiosInstance
    .get('/'+id+'/game/'+gameId)
    .then((res) => {
      console.log(res)
      dispatch(getSpinByIDSuccess(res));
    })
    .catch((e) => {
      console.log(e.response)
      dispatch(getSpinByIDFailed(e));
    });
};

// get_next_game
export const getNextGameRequest = () => ({
  type: GET_NEXT_GAME_REQUEST
});

export const getNextGameSuccess = response => ({
  type: GET_NEXT_GAME_SUCCESS,
  response
});

export const getNextGameFailed = error => ({
  type: GET_NEXT_GAME_FAILED,
  error
});

export const fetchgetNextGame = (id=1) => (dispatch) => {
  dispatch(getNextGameRequest());
  axiosInstance
    .get('/'+id+'/nextGame')
    .then((res) => {
      console.log(res)
      dispatch(getNextGameSuccess(res));
    })
    .catch((e) => {
      console.log(e.response)
      dispatch(getNextGameFailed(e));
    });
};

// get_last_n_finished_spins
export const getLastNFinishedSpinsRequest = () => ({
  type: GET_LAST_N_FINISHED_SPINS_REQUEST
});

export const getLastNFinishedSpinsSuccess = response => ({
  type: GET_LAST_N_FINISHED_SPINS_SUCCESS,
  response
});

export const getLastNFinishedSpinsFailed = error => ({
  type: GET_LAST_N_FINISHED_SPINS_FAILED,
  error
});

export const fetchgetLastNFinishedSpins = (id=1,uuid) => (dispatch) => {
  dispatch(getLastNFinishedSpinsRequest());
  axiosInstance
    .get('/'+id+'/history/')
    .then((res) => {
      console.log(res)
      dispatch(getLastNFinishedSpinsSuccess(res));
    })
    .catch((e) => {
      console.log(e.response)
      dispatch(getLastNFinishedSpinsFailed(e));
    });
};

// get_last_n_finished_spins
export const getListofUpcomingSpinsRequest = () => ({
  type: GET_LIST_OF_UPCOMING_SPINS_REQUEST
});

export const getListofUpcomingSpinsSuccess = response => ({
  type: GET_LIST_OF_UPCOMING_SPINS_SUCCESS,
  response
});

export const getListofUpcomingSpinsFailed = error => ({
  type: GET_LIST_OF_UPCOMING_SPINS_FAILED,
  error
});

export const fetchgetListofUpcomingSpins = (id=1,uuid) => (dispatch) => {
  dispatch(getListofUpcomingSpinsRequest());
  axiosInstance
    .get('/'+id+'/scheduledGames/')
    .then((res) => {
      console.log(res)
      dispatch(getListofUpcomingSpinsSuccess(res));
    })
    .catch((e) => {
      console.log(e.response)
      dispatch(getListofUpcomingSpinsFailed(e));
    });
};


// last_n_spin_statistics
export const lastNSpinStatisticsRequest = () => ({
  type: LAST_N_SPIN_STATISTICS_REQUEST
});

export const lastNSpinStatisticsSuccess = response => ({
  type: LAST_N_SPIN_STATISTICS_SUCCESS,
  response
});

export const lastNSpinStatisticsFailed = error => ({
  type: LAST_N_SPIN_STATISTICS_FAILED,
  error
});

export const fetchlastNSpinStatistics = (id=1,uuid) => (dispatch) => {
  dispatch(lastNSpinStatisticsRequest());
  axiosInstance
    .get('/'+id+'/stats?limit=200')
    .then((res) => {
      console.log(res)
      dispatch(lastNSpinStatisticsSuccess(res));
    })
    .catch((e) => {
      console.log(e.response)
      dispatch(lastNSpinStatisticsFailed(e));
    });
};

// get_wheel_configuration
export const getWheelConfigurationRequest = () => ({
  type: GET_WHEEL_CONFIGURATION_REQUEST
});

export const getWheelConfigurationSuccess = response => ({
  type: GET_WHEEL_CONFIGURATION_SUCCESS,
  response
});

export const getWheelConfigurationFailed = error => ({
  type: GET_WHEEL_CONFIGURATION_FAILED,
  error
});

export const fetchgetWheelConfiguration = (id=1) => (dispatch) => {
  dispatch(getWheelConfigurationRequest());
  axiosInstance
    .get('/'+id+'/configuration')
    .then((res) => {
      console.log(res)
      dispatch(getWheelConfigurationSuccess(res));
    })
    .catch((e) => {
      console.log(e.response)
      dispatch(getWheelConfigurationFailed(e));
    });
};


