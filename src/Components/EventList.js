import React from 'react';

const EventList = ({
    items,
    time,
    game
}) =>{
    return (
        <ul id="event_box" className="list-group">
            {
                items.map((item, index)=>(
                    <li key={index} className="list-group-item">{item}</li>
                ))
            }
             {
                time > 0 && <li  className="list-group-item"> Game {game} will start in {time} sec</li>
            }
        </ul>
    )
}

export default EventList;