import React from 'react';
const styles = {
    wrapperStyle: {
        display: 'flex'
    },
    itemStyle: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        color: 'white',
        padding: '7px 7px'
    }
}

const SlotItemList = ({
    items,
    colors
}) =>{
    return (
        <div style={styles.wrapperStyle}>
        <h6 style={{marginTop: '5px', marginRight: '10px'}}> Slot </h6>
            {
                items.map((item, index)=>(
                    <div key={index} style={{...styles.itemStyle, backgroundColor: colors[item.result]}}>{item.result}</div>
                ))
            }
        </div>
    )
}

export default SlotItemList;