import React from 'react';
const styles = {
    wrapperStyle: {
        display: 'flex',
        width: '100%',
        flexDirection: 'column',
        border: '1px solid #ccc',
        padding: '10px',
        backgroundColor: '#f5f5f5',
        borderRadius: '5px'

    },
    itemStyle: {
    }
}

const LogList = ({
    items
}) =>{
    return (
        <div style={styles.wrapperStyle}>
            {
                items.map((item, index)=>(
                    <div key={index}>
                        <p style={{marginBottom: '5px', fontSize: '14px'}}>{item}</p>
                    </div>
                ))
            }
        </div>
    )
}

export default LogList;