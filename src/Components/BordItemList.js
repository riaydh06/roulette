import React from 'react';
const styles = {
    wrapperStyle: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    itemStyle: {
        display: 'flex',
        width: '35px',
        height: '35px',
        justifyContent: 'center',
        alignItems: 'center',
        color: 'white',
        borderRadius: '6px'
    }
}

const BordItemList = ({
    items,
    colors
}) =>{
    return (
        <div style={styles.wrapperStyle}>
            {
                items.map((item, index)=>(
                    <div key={index} style={{...styles.itemStyle, backgroundColor: colors[item]}}>{item}</div>
                ))
            }
        </div>
    )
}

export default BordItemList;