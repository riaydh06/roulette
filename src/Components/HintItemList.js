import React from 'react';
const styles = {
    wrapperStyle: {
        display: 'flex'
    },
    itemStyle: {
        display: 'flex',
        minWidth: '29px',
        padding: '10px 7px',
        justifyContent: 'center',
        alignItems: 'center',
        color: 'black'
    }
}

const HintItemList = ({
    items
}) =>{
    return (
        <div style={styles.wrapperStyle}>
        <h6 style={{marginTop: '5px', marginRight: '10px'}}> Hits </h6>
            {
                items.map((item, index)=>(
                    <div key={index} style={{...styles.itemStyle, backgroundColor: index<5 ? '#D9EDF7':  index>31?'#F2DEDE' :'#ffffff'}}>{ item.count}</div>
                ))
            }
        </div>
    )
}

export default HintItemList;